require 'sinatra'
require 'haml'
require 'json'

set :haml, format: :html5

GIT_LOCATION = 'git://github.com/hammerdr/rails.git'
unless Dir.exists?('tmp/repository')
  `git clone #{GIT_LOCATION} tmp/repository`
end

def tags
  `cd tmp/repository && git fetch --tags && git tag`.split
end

def refs
  `cd tmp/repository && git fetch && git branch -r`.split + tags
end

get '/' do
  locals = Hash[git_location: GIT_LOCATION, refs: refs]
  haml :index, locals: locals
end

post '/commitin' do
  content_type :json
  refs
  result = `cd tmp/repository && git commitin #{params[:commit]} #{params[:branch]}`
  { :commitin => result.chomp }.to_json
end
